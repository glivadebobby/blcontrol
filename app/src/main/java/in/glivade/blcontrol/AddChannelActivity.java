package in.glivade.blcontrol;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import in.glivade.blcontrol.db.DbHandler;
import in.glivade.blcontrol.model.Channel;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class AddChannelActivity extends AppCompatActivity implements View.OnClickListener {

    private Context context;
    private Toolbar toolbar;
    private EditText editTextCode, editTextName;
    private Button buttonSubmit;
    private AnimationDrawable animationDrawable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_channel);
        initObjects();
        initCallbacks();
        initToolbar();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (animationDrawable != null && !animationDrawable.isRunning()) {
            animationDrawable.start();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (animationDrawable != null && animationDrawable.isRunning()) {
            animationDrawable.stop();
        }
    }

    @Override
    public void onClick(View view) {
        if (view == buttonSubmit) {
            processAddChannel();
        }
    }

    private void initObjects() {
        toolbar = findViewById(R.id.toolbar);
        editTextCode = findViewById(R.id.input_code);
        editTextName = findViewById(R.id.input_name);
        buttonSubmit = findViewById(R.id.btn_submit);

        context = this;
        animationDrawable = (AnimationDrawable) findViewById(R.id.add_channel).getBackground();
    }

    private void initCallbacks() {
        buttonSubmit.setOnClickListener(this);
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void processAddChannel() {
        String code = editTextCode.getText().toString().trim();
        String name = editTextName.getText().toString().trim();
        if (validateInput(code, name)) {
            Toast.makeText(this, "Channel added", Toast.LENGTH_SHORT).show();
            new DbHandler(context).addChannel(new Channel(code, name));
            editTextCode.getText().clear();
            editTextName.getText().clear();
        }
    }

    private boolean validateInput(String code, String name) {
        if (code.isEmpty()) {
            editTextCode.requestFocus();
            editTextCode.setError(getString(R.string.error_empty));
            return false;
        } else if (name.isEmpty()) {
            editTextName.requestFocus();
            editTextName.setError(getString(R.string.error_empty));
            return false;
        }
        return true;
    }
}
