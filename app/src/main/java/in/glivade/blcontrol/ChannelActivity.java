package in.glivade.blcontrol;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import in.glivade.blcontrol.adapter.ChannelAdapter;
import in.glivade.blcontrol.db.DbHandler;
import in.glivade.blcontrol.model.Channel;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ChannelActivity extends AppCompatActivity implements View.OnClickListener,
        ChannelAdapter.ChannelCallback {

    private Context context;
    private Toolbar toolbar;
    private RecyclerView viewChannels;
    private FloatingActionButton buttonAdd;
    private List<Channel> channelList;
    private ChannelAdapter channelAdapter;
    private AnimationDrawable animationDrawable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_channel);
        initObjects();
        initCallbacks();
        initToolbar();
        initRecyclerView();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (animationDrawable != null && !animationDrawable.isRunning()) {
            animationDrawable.start();
        }
        populateData();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (animationDrawable != null && animationDrawable.isRunning()) {
            animationDrawable.stop();
        }
    }

    @Override
    public void onClick(View view) {
        if (view == buttonAdd) {
            startActivity(new Intent(this, AddChannelActivity.class));
        }
    }

    @Override
    public void onDeleteClick(int position) {
        Toast.makeText(this, "Channel removed", Toast.LENGTH_SHORT).show();
        new DbHandler(this).removeChannel(channelList.get(position).getCode());
        populateData();
    }

    private void initObjects() {
        toolbar = findViewById(R.id.toolbar);
        viewChannels = findViewById(R.id.channels);
        buttonAdd = findViewById(R.id.fab_add);

        context = this;
        channelList = new ArrayList<>();
        channelAdapter = new ChannelAdapter(channelList, this);
        animationDrawable = (AnimationDrawable) findViewById(R.id.channel).getBackground();
    }

    private void initCallbacks() {
        buttonAdd.setOnClickListener(this);
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initRecyclerView() {
        viewChannels.setLayoutManager(new LinearLayoutManager(context));
        viewChannels.setAdapter(channelAdapter);
    }

    private void populateData() {
        channelList.clear();
        channelList.addAll(new DbHandler(context).getChannel());
        channelAdapter.notifyDataSetChanged();
        if (channelList.isEmpty()) {
            Toast.makeText(context, "Please add some channels", Toast.LENGTH_SHORT).show();
        }
    }
}
