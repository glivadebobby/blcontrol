package in.glivade.blcontrol;

import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.github.douglasjunior.bluetoothclassiclibrary.BluetoothDeviceDecorator;
import com.github.douglasjunior.bluetoothclassiclibrary.BluetoothService;

import java.util.ArrayList;
import java.util.List;

import in.glivade.blcontrol.app.MyPreference;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ConnectActivity extends AppCompatActivity implements View.OnClickListener,
        BluetoothService.OnBluetoothScanCallback {

    private Toolbar toolbar;
    private TextView textViewDiscover;
    private Button buttonScan, buttonConnect;
    private AnimationDrawable mAnimationDrawable;
    private BluetoothAdapter bluetoothAdapter;
    private BluetoothService bluetoothService;
    private List<BluetoothDeviceDecorator> bluetoothDeviceDecoratorList;
    private MyPreference preference;
    private boolean scanning = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connect);
        initObjects();
        initToolbar();
        initCallbacks();
        initDevices();
        invalidateDiscoverText();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mAnimationDrawable != null && !mAnimationDrawable.isRunning()) {
            mAnimationDrawable.start();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mAnimationDrawable != null && mAnimationDrawable.isRunning()) {
            mAnimationDrawable.stop();
        }
    }

    @Override
    public void onClick(View v) {
        if (v == buttonScan) {
            processScanning();
        } else if (v == buttonConnect) {
            promptConnectDeviceDialog();
        }
    }

    @Override
    public void onDeviceDiscovered(BluetoothDevice bluetoothDevice, int i) {
        BluetoothDeviceDecorator deviceDecorator = new BluetoothDeviceDecorator(bluetoothDevice, i);
        int index = bluetoothDeviceDecoratorList.indexOf(deviceDecorator);
        if (index < 0) bluetoothDeviceDecoratorList.add(deviceDecorator);
        else {
            bluetoothDeviceDecoratorList.get(index).setDevice(bluetoothDevice);
            bluetoothDeviceDecoratorList.get(index).setRSSI(i);
        }
        invalidateDiscoverText();
    }

    @Override
    public void onStartScan() {
        Toast.makeText(this, "Scanning devices..", Toast.LENGTH_SHORT).show();
        scanning = true;
        setScanText();
    }

    @Override
    public void onStopScan() {
        Toast.makeText(this, "Stopped scanning", Toast.LENGTH_SHORT).show();
        scanning = false;
        setScanText();
    }

    private void initObjects() {
        toolbar = findViewById(R.id.toolbar);
        textViewDiscover = findViewById(R.id.txt_discover);
        buttonScan = findViewById(R.id.btn_scan);
        buttonConnect = findViewById(R.id.btn_connect);

        mAnimationDrawable = (AnimationDrawable) findViewById(R.id.connect).getBackground();
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        bluetoothService = BluetoothService.getDefaultInstance();
        bluetoothDeviceDecoratorList = new ArrayList<>();
        preference = new MyPreference(this);
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initCallbacks() {
        buttonScan.setOnClickListener(this);
        buttonConnect.setOnClickListener(this);
        bluetoothService.setOnScanCallback(this);
    }

    private void initDevices() {
        for (BluetoothDevice device : bluetoothAdapter.getBondedDevices()) {
            bluetoothDeviceDecoratorList.add(new BluetoothDeviceDecorator(device, 0));
        }
    }

    private void processScanning() {
        if (scanning) {
            bluetoothService.stopScan();
        } else {
            bluetoothService.startScan();
        }
    }

    private void setScanText() {
        if (scanning) {
            buttonScan.setText(getString(R.string.btn_stop_scanning));
        } else {
            buttonScan.setText(getString(R.string.btn_start_scanning));
        }
    }

    private void invalidateDiscoverText() {
        textViewDiscover.setText(getResources().getQuantityString(R.plurals.devices,
                bluetoothDeviceDecoratorList.size(), bluetoothDeviceDecoratorList.size()));
    }

    private void promptConnectDeviceDialog() {
        String[] devices = new String[bluetoothDeviceDecoratorList.size()];
        for (int i = 0; i < bluetoothDeviceDecoratorList.size(); i++) {
            BluetoothDeviceDecorator device = bluetoothDeviceDecoratorList.get(i);
            devices[i] = device.getName();
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Connect to Device");
        builder.setItems(devices, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                BluetoothDeviceDecorator deviceDecorator = bluetoothDeviceDecoratorList.get(which);
                preference.setMac(deviceDecorator.getAddress());
                preference.setName(deviceDecorator.getName());
                bluetoothService.connect(deviceDecorator.getDevice());
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
