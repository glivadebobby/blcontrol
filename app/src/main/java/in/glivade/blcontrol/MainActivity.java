package in.glivade.blcontrol;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.github.douglasjunior.bluetoothclassiclibrary.BluetoothService;
import com.github.douglasjunior.bluetoothclassiclibrary.BluetoothStatus;

import java.util.ArrayList;
import java.util.Locale;

import in.glivade.blcontrol.app.MyPreference;
import in.glivade.blcontrol.db.DbHandler;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends AppCompatActivity implements View.OnClickListener,
        BluetoothService.OnBluetoothEventCallback {

    private static final int REQUEST_VOICE_INPUT = 4;
    private Toolbar toolbar;
    private ImageView mImageViewMic;
    private AnimationDrawable mAnimationDrawable;
    private BluetoothAdapter bluetoothAdapter;
    private BluetoothService bluetoothService;
    private MyPreference preference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initObjects();
        initToolbar();
        initCallbacks();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mAnimationDrawable != null && !mAnimationDrawable.isRunning()) {
            mAnimationDrawable.start();
        }
        initConnection();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mAnimationDrawable != null && mAnimationDrawable.isRunning()) {
            mAnimationDrawable.stop();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_connect) {
            startActivity(new Intent(this, ConnectActivity.class));
            return true;
        } else if (item.getItemId() == R.id.action_channels) {
            startActivity(new Intent(this, ChannelActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_VOICE_INPUT && resultCode == RESULT_OK && null != data) {
            ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            if (!result.isEmpty()) {
                String speechData = result.get(0);
                String code = new DbHandler(this).getCode(speechData);
                if (code != null) {
                    bluetoothService.write(code.getBytes());
                } else {
                    Toast.makeText(this, "Sorry, couldn\'t find the channel", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(this, "Sorry, couldn\'t recognize your voice", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onClick(View v) {
        if (v == mImageViewMic) {
            if (preference.getMac() == null) {
                Toast.makeText(this, "Not connected to any device", Toast.LENGTH_SHORT).show();
            } else promptSpeechInput();
        }
    }

    @Override
    public void onDataRead(byte[] bytes, int i) {
        Toast.makeText(this, bytes.toString(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onStatusChange(BluetoothStatus bluetoothStatus) {
        if (bluetoothStatus == BluetoothStatus.CONNECTED) {
            Toast.makeText(this, "Connected to " + preference.getName(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onDeviceName(String s) {
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onToast(String s) {
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDataWrite(byte[] bytes) {
        Toast.makeText(this, bytes.toString(), Toast.LENGTH_SHORT).show();
    }

    private void initObjects() {
        toolbar = findViewById(R.id.toolbar);
        mImageViewMic = findViewById(R.id.img_mic);

        mAnimationDrawable = (AnimationDrawable) findViewById(R.id.main).getBackground();
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        bluetoothService = BluetoothService.getDefaultInstance();
        preference = new MyPreference(this);
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
    }

    private void initCallbacks() {
        mImageViewMic.setOnClickListener(this);
        bluetoothService.setOnEventCallback(this);
    }

    private void initConnection() {
        if (preference.getMac() == null) {
            Toast.makeText(this, "Not connected to any device", Toast.LENGTH_SHORT).show();
        } else {
            BluetoothDevice bluetoothDevice = bluetoothAdapter.getRemoteDevice(preference.getMac());
            bluetoothService.connect(bluetoothDevice);
        }
    }

    private void promptSpeechInput() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        startActivityForResult(intent, REQUEST_VOICE_INPUT);
    }
}
