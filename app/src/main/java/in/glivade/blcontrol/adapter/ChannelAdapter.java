package in.glivade.blcontrol.adapter;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import in.glivade.blcontrol.R;
import in.glivade.blcontrol.model.Channel;

public class ChannelAdapter extends RecyclerView.Adapter<ChannelAdapter.ChannelHolder> {

    private List<Channel> channelList;
    private ChannelCallback callback;

    public ChannelAdapter(List<Channel> channelList, ChannelCallback callback) {
        this.channelList = channelList;
        this.callback = callback;
    }

    @Override
    public ChannelHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ChannelHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_channel,
                parent, false), callback);
    }

    @Override
    public void onBindViewHolder(ChannelHolder holder, int position) {
        Channel channel = channelList.get(position);
        holder.textViewCode.setText(channel.getCode());
        holder.textViewName.setText(channel.getName());
    }

    @Override
    public int getItemCount() {
        return channelList.size();
    }

    public interface ChannelCallback {
        void onDeleteClick(int position);
    }

    class ChannelHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView textViewCode, textViewName;
        ImageView imageViewDelete;
        ChannelCallback callback;

        ChannelHolder(View itemView, ChannelCallback callback) {
            super(itemView);
            textViewCode = itemView.findViewById(R.id.txt_code);
            textViewName = itemView.findViewById(R.id.txt_name);
            imageViewDelete = itemView.findViewById(R.id.img_delete);
            this.callback = callback;
            imageViewDelete.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (view == imageViewDelete) callback.onDeleteClick(getLayoutPosition());
        }
    }
}
