package in.glivade.blcontrol.app;

import android.app.Application;

import com.github.douglasjunior.bluetoothclassiclibrary.BluetoothClassicService;
import com.github.douglasjunior.bluetoothclassiclibrary.BluetoothConfiguration;
import com.github.douglasjunior.bluetoothclassiclibrary.BluetoothService;

import java.util.UUID;

import in.glivade.blcontrol.R;

public class AppController extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        BluetoothConfiguration configuration = new BluetoothConfiguration();
        configuration.bluetoothServiceClass = BluetoothClassicService.class;
        configuration.context = getApplicationContext();
        configuration.bufferSize = 1024;
        configuration.characterDelimiter = '\n';
        configuration.deviceName = getString(R.string.app_name);
        configuration.callListenersInMainThread = true;
        configuration.uuid = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
        BluetoothService.init(configuration);
    }
}
