package in.glivade.blcontrol.app;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;

public class MyPreference {

    private static final String PREF_DEVICE = "device";
    private static final String MAC = "mac";
    private static final String NAME = "name";
    private SharedPreferences mPreferencesDevice;

    public MyPreference(Context context) {
        mPreferencesDevice = context.getSharedPreferences(PREF_DEVICE, Context.MODE_PRIVATE);
    }

    public String getMac() {
        return mPreferencesDevice.getString(encode(MAC), null);
    }

    public void setMac(String mac) {
        mPreferencesDevice.edit().putString(encode(MAC), mac).apply();
    }

    public String getName() {
        return mPreferencesDevice.getString(encode(NAME), null);
    }

    public void setName(String name) {
        mPreferencesDevice.edit().putString(encode(NAME), name).apply();
    }

    private String encode(String data) {
        return Base64.encodeToString(data.getBytes(), Base64.NO_WRAP);
    }
}
