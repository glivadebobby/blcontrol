package in.glivade.blcontrol.db;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import in.glivade.blcontrol.model.Channel;

public class DbHandler extends SQLiteOpenHelper {

    private static final String DB_BLCONTROL = "blcontrol";
    private static final int DB_VERSION_BLCONTROL = 1;
    private static final String TABLE_CHANNEL = "channel";
    private static final String KEY_CODE = "code";
    private static final String KEY_NAME = "name";

    public DbHandler(Context context) {
        super(context, DB_BLCONTROL, null, DB_VERSION_BLCONTROL);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        createChannelTable(sqLiteDatabase);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        String dropTable = "DROP TABLE IF EXISTS ";
        sqLiteDatabase.execSQL(dropTable + TABLE_CHANNEL);
        onCreate(sqLiteDatabase);
    }

    private void createChannelTable(SQLiteDatabase sqLiteDatabase) {
        String createTable = "CREATE TABLE " + TABLE_CHANNEL + " ("
                + KEY_CODE + " TEXT PRIMARY KEY, " + KEY_NAME + " TEXT" + ")";
        sqLiteDatabase.execSQL(createTable);
    }

    public void addChannel(Channel channel) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_CODE, channel.getCode());
        values.put(KEY_NAME, channel.getName());
        db.insertWithOnConflict(TABLE_CHANNEL, null, values, SQLiteDatabase.CONFLICT_REPLACE);
        db.close();
    }

    public List<Channel> getChannel() {
        List<Channel> channels = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + TABLE_CHANNEL;
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                String code = cursor.getString(0);
                String name = cursor.getString(1);
                channels.add(new Channel(code, name));
            } while (cursor.moveToNext());
            cursor.close();
        }
        db.close();
        return channels;
    }

    public String getCode(String channelName) {
        String selectQuery = "SELECT * FROM " + TABLE_CHANNEL;
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                String code = cursor.getString(0);
                String name = cursor.getString(1);
                if (name.equalsIgnoreCase(channelName)) {
                    cursor.close();
                    db.close();
                    return code;
                }
            } while (cursor.moveToNext());
            cursor.close();
        }
        db.close();
        return null;
    }

    public void removeChannel(String code) {
        SQLiteDatabase db = getWritableDatabase();
        db.delete(TABLE_CHANNEL, KEY_CODE + " = ?", new String[]{code});
        db.close();
    }
}
